# CS 179

### Fun ways to learn Python

* [Fundamentals of Python textbook](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html) - Runestone Academy books are interactive and free!
* [EdaBit](https://edabit.com/tutorial/python) - Lessons and hints and interactive interpretters
* [Python Anywhere](https://www.pythonanywhere.com/registration/register/beginner/) - you can even deploy a Python web application (website) here
* [Interactive Python Debugger](https://pythontutor.com/python-debugger.html#) - A debugger visualizes your code as it processes it one line at a time

## Welcome to CISC 179: Python Programming: Spring 2023 - CISC 179

Modules   Syllabus   Discussions   Grades

I'm Hobson Lane. My goal is to help each one of you become a successful Python programmer. Welcome to CISC 179 - Python Programming!

You will be using the Canvas Learning Management System (LMS) to manage all your assignments, exercises, and discussions. You will need to complete one Module (chapter and assignment) per week.

To get started:

1. Add my office hours to your calendar:
  * Every Wednesday at 6PM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=M3VibDg3aG5ycXBoOTdnYnRscmtnMG41cW9fMjAyMzAyMDlUMDIwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&amp;tmsrc=hobson%40totalgood.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
  * Every Thursday at 5PM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=MmF0cHF2ZDM0azhyYzVuY2c2dHRmNGN0bmZfMjAyMzAyMTBUMDEwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&amp;tmsrc=hobson%40totalgood.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
2. Download Kenneth Lambert's free e-book [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=1)
3. Read Chapter 1 of [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=0)
4. Optionally, sign up for the free Runstone Academy [interactive Python textbook](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html)
5. Create a [GitLab.com](gitlab.com) account and head over to my git repository where I keep some useful Python learning resources (http://gitlab.com/tangibleai/community/cs179/).
6. Head over to the course Modules in Canvas

Kenneth Lambert's free e-book [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=1)
3. Read Chapter 1 of [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=0) contains everything you need to succeed in this course.


.
